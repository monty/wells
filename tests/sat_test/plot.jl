colors = ["red", "blue", "green", "orange", "magenta", "cyan", "brown", "pink", "lime", "navy", "maroon", "yellow", "olive", "springgreen", "teal", "coral", "#e6beff", "beige", "purple", "#4B6F44", "#9F4576"]
pr = ["confined_0", "confined_1", "confined_3", "unconfined_0", "unconfined_1", "unconfined_3"]
ll = []
for (i, f) in enumerate(pr)
	x = DelimitedFiles.readdlm("$f.s_point"; header=true)[1][:,1]
	y = DelimitedFiles.readdlm("$f.s_point"; header=true)[1][:,5]
	if i < 4
		line_style = [:solid]
	else
		line_style = [:dash]
	end
	l = Gadfly.layer(x=x, y=y, Gadfly.Geom.line(), Gadfly.Theme(line_width=3Gadfly.pt, line_style=line_style, default_color=Gadfly.color(colors[i])))
	push!(ll, l)
	display(Gadfly.plot(l, Gadfly.Scale.x_log10())); println()
end
display(Gadfly.plot(ll..., Gadfly.Scale.x_log10(), Gadfly.Guide.manual_color_key("Legend", pr, colors[1:length(pr)]))); println()